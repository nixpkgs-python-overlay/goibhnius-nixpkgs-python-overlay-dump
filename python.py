import xmlrpclib
import os
import marshal
import pprint
from optparse import OptionParser

# optionally cache api results
class Cache:
  def __init__(self, use_cache):
    self.use_cache = use_cache

  def file(self, what):
    return "%s.cache" % what

  def write(self, what, item):
    f = open(self.file(what),'w')
    marshal.dump(item, f)
    f.close()
    print "dumping %s" % what

  def read(self, what, default):
    name = self.file(what)
    if not self.use_cache:
      return default
    else:
      try:
        f = file(name, 'r')
        r = marshal.load(f)
        f.close()
        return r
      except Exception, e:
        print "failed reading from cache file %s" % name
        return default

  def withCache(self, what, default, fun):
    cached = self.read(what, default)
    try:
      cached = fun(cached)
    finally:
      # write partial result
      if self.use_cache:
        self.write(what, cached)
    return cached

def namesAndVersionsByNameList(client, cache, show_hidden):
  # takes about 3 hours?
  # newest version should be last
  def fetchNameList(list):
      if len(list) > 0:
        print "using name list from cache"
        return list
      print "getting name list .."
      list = client.list_packages()
      print "got %d names " % len(list)
      return list

  names = cache.withCache("names", [], fetchNameList)

  def fetchReleases(byName):
    nr = 0
    for package_name in names:
      if byName.has_key(package_name):
        if not cache.use_cache:
          fail_duplicate_name() # func does not exist - raise error
        # else:
          # print "%s from cache " % package_name
      else:
        print "getting package versions %s (%d of %d - %d %%)" % (package_name, nr, len(names), 100 * nr / len(names))
        
        byName[package_name] = client.package_releases(package_name, show_hidden)
        
      nr += 1
    return byName

  return cache.withCache("byName", {}, fetchReleases)

def namesAndVersionsBySearch(client, cache, show_hidden):
  # using search. faster but less names !?
  # newest version should be last

  def searchResult(r):
    if len(r) > 0:
      print "got package list from cache"
      return r
    print "fetching package list using search"
    return client.search({})

  r = cache.withCache("searchResult", {}, searchResult)
  byName = {}
  for v in r:
    if not byName.has_key(v['name']):
      byName[v['name']] = []
    byName[v['name']].append(v)

  for package_name in byName.keys():
    byName[package_name] = [x['version'] for x in byName[package_name] ]
  return byName

def get_packages(cache, show_hidden, use_search):
  # API see http://wiki.python.org/moin/PyPiXmlRpc?action=show&redirect=CheeseShopXmlRpc

  url = 'http://pypi.python.org/pypi'
  print "connecting to %s" % (url)
  client = xmlrpclib.ServerProxy(url)

  if use_search:
    namesAndVersions = namesAndVersionsBySearch(client, cache, show_hidden)
  else: namesAndVersions = namesAndVersionsByNameList(client, cache, show_hidden)
  print "names count: %d" %  len(namesAndVersions)

  # list like { 'plonetheme.fui': ['2.2.1'] }
  # now get details:
  def fetchPackageDetails(dict):
    fetch = []
    # find out which packages must be fetched
    for name in namesAndVersions.keys():
       for version in namesAndVersions.get(name):
          if not dict.has_key(name):
             dict[name] = {}
          if not dict.get(name).has_key(version):
             fetch.append([ name, version ])

    # fetch them
    done = 0
    for n_v in fetch:
      done +=1
      print "fetching details %s %s %.2f %" % (n_v[0], n_v[1], 100.00 * done / len(fetch))
      dict.get(n_v[0])[n_v[1]] = client.release_data(name, version)
    return dict
    
  details = cache.withCache("package-details", {}, fetchPackageDetails)
  name = details.keys()[0]
  print name, details.get(name)

def main():
  # parse command line options
  parser = OptionParser()
  parser.add_option("","--include-hidden", action="store_true", dest="include_hidden", help="also add hidden packages")
  parser.add_option("-c","--use-cache", action="store_true", dest="use_cache", help="use cache (for testing and development)")
  parser.add_option("-s","--use-search", action="store_true", dest="use_search", help="use xmlrpclib function search to get package and version lists (is faster - but maybe also less accurate)")
  (options, args) = parser.parse_args()

  if options.use_cache:
    print "using / creating cache"
  cache = Cache(options.use_cache)

  get_packages(cache, options.include_hidden != None, options.use_search != None)


if __name__ == "__main__":
  main()
